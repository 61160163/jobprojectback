var mongoose = require('mongoose');
 
var imageSchema = new mongoose.Schema({
    nameSurname : String,
    address: String,
    experience : String,
    educational : String,
    salary: String,
    license :
    {
        data: Buffer,
        contentType: String
    },
    resume :
    {
        data: Buffer,
        contentType: String
    }
});
 
//Image is a model which has a schema imageSchema
 
module.exports = new mongoose.model('Image', imageSchema);