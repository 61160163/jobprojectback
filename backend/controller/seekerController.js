const seekerController = {
  userList: [
    {
      "id": 1
    },
    {
      "id": 2
      
    }
  ],
  listId: 3,
  add(user) {
    user.id = this.listId++
    this.userList.push(user)
    return user
  },
  update(user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
    return user
  },
  delete(id) {
    const index = this.userList.findIndex(item => item.id === parseInt(id))
    this.userList.splice(index, 1)
    return { id }
  },
  gets() {
    return [...this.userList]
  },
  get(id) {
    const user = this.userList.find(item => item.id === parseInt(id))
    return user
  }
}
module.exports = seekerController
