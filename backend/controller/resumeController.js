const resume = require('../models/resume')
const resumeController = {
  async addresume(req,res){
    const payload = req.body
    const resumes = new resume(payload)
    try{
        await resumes.save()
         res.json(resumes)
    }catch(err){
         res.status(500).send(err)
    }
},
async updateresume(req, res) {
    const payload = req.body
    try {
      const resumes = await resume.updateOne({ _id: payload._id }, payload)
      res.json(resumes)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteresume(req, res) {
    const { id } = req.params
    try {
      const resumes = await resume.deleteOne({ _id: id })
      res.json(resumes)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getresume(req, res) {
    try {
      const resumes = await resume.find({})
      res.json(resumes)
      console.log(resumes[0].username+" "+resumes[0].password+" "+resumes.length)
      

    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getresumeID(req, res) {
    const { id } = req.params
    try {
      const resumes = await resume.findById(id)
      res.json(resumes)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async checkresume(req, res) {
    
  }
}
module.exports = resumeController
