const Company = require('../models/company')
const companyController = {
  async addCom(req, res) {
    const payload = req.body
    const companies = new Company(payload)
    try {
      await companies.save()
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateCom(req, res) {
    const payload = req.body
    try {
      const companies = await Company.updateOne({ _id: payload._id }, payload)
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteCom(req, res) {
    const { id } = req.params
    try {
      const companies = await Company.deleteOne({ _id: id })
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCom(req, res) {
    try {
      const companies = await Company.find({})
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getComID(req, res) {
    const { id } = req.params
    try {
      const companies = await Company.findById(id)
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = companyController
