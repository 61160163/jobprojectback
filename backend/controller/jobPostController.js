const JobPosting = require('../models/jobPosting')
const jobPostingController = {
  async addJob(req, res) {
    const payload = req.body
    const jobPosts = new JobPosting(payload)
    try {
      await jobPosts.save()
      res.json(jobPosts)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateJob(req, res) {
    const payload = req.body
    try {
      const jobPosts = await JobPosting.updateOne({ _id: payload._id }, payload)
      res.json(jobPosts)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteJob(req, res) {
    const { id } = req.params
    try {
      const jobPosts = await JobPosting.deleteOne({ _id: id })
      res.json(jobPosts)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJob(req, res) {
    try {
      const jobPosts = await JobPosting.find({})
      res.json(jobPosts)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobID(req, res) {
    const { id } = req.params
    try {
      const jobPosts = await JobPosting.findById(id)
      res.json(jobPosts)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async search(req, res) {
    console.log('test')
    const body = req.body.searchInput
    console.log(body)
    const jobPosts = await JobPosting.find({ position: {$regex: '.*' + body + '.*'} }).exec();
    res.json(jobPosts)
  },
  async getSearch(req, res) {
    try {
      const jobPosts = await JobPosting.find({})
      const param = req.body.searchInput
      var newjob
      for (let i = 0; i < jobPosts.length; i++) {
        pos = jobPosts[i].position
        console.log(pos)
        if (pos.includes(param)) {
          newjob = jobPosts[i]
          break;
        }
      }
      console.log(param)
      res.json(newjob)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = jobPostingController
