const express = require('express')
const router = express.Router()
const loginController = require('../controller/loginController')

/* GET users listing. */
router.get('/',loginController.getLogin)
router.get('/:id',loginController.getLoginID)
router.post('/',loginController.addLogin)
router.put('/',loginController.updateLogin)
router.delete('/:id',loginController.deleteLogin)
router.get('/checklogin',loginController.checkLogin)

module.exports = router
