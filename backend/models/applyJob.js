const mongoose = require('mongoose');

const applyJobSchema = new mongoose.Schema({
    nameSurname: String,
    position: String,
    address: String,
    experience: String,
    educational: String,
    salary: String,
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('applyJob', applyJobSchema);