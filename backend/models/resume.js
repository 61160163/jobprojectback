const mongoose = require('mongoose');

const resumeSchema = new mongoose.Schema({
    name: String,
    lastname: String,
    birthday: String,
    idcard: String,
    age: String,
    gender: String,
    phonenumber: String,
    email: String,
    address: String
});

module.exports = mongoose.model('resume', resumeSchema);