**จำเป็นอย่างมากสำหรับbackend ต้องติดตั้ง https://fastdl.mongodb.org/windows/mongodb-windows-x86_64-4.4.4-signed.msi
ครั้งแรกครั้งเดียว

จำเป็นต้องมี Visual Studio code ในการเขียนโค้ด(ใช้อย่างอื่นได้ตามความถนัด), 
Postman ในการเช็คแต่ละAPIว่าสามารถทำ CRUDได้ไหม, 
Robo3T หรือ mongoDB Compass เพื่อที่จะดูและบันทึกDB ไปที่ mongodb://localhost:27017/my_database

run database โดยใช้คำสั่ง npm run start

api start http://localhost:3000/

ส่วนของ company http://localhost:3000/company/